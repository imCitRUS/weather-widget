export * as Icon from "./Icon"
export { default as MainTab } from "./MainTab.vue"
export { default as SettingsTab } from "./SettingsTab.vue"
