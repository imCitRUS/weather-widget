export type Tab = "main" | "settings"

export interface Weather {
    description: string
    icon: string
}

export interface WeatherData {
    weather: Array<Weather>
    main: {
        temp: number
        feels_like: number
        pressure: number
        humidity: number
    }
    visibility: number
    wind: {
        speed: number
        deg: number
    }
    sys: {
        country: string
    }
    name: string
}

export interface CityInfo {
    name: string
    country: string
    icon: string
    temp: number
    tempFeelsLike: number
    pressure: number
    humidity: number
    windSpeed: number
    windDirection: string
    dewPoint: number
    visibility: number
    description: string
}
